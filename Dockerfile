# Use latest jboss/base-jdk:7 image as the base
FROM weepee/centos6

LABEL name="CentOS Base Image"
LABEL vendor="CentOS"
LABEL license="GPLv2"
LABEL build-date="2016-03-31"

RUN yum -y install bash ; yum clean all

ADD echo.sh ./ 

# Set the default command to run on boot
# This will boot WildFly in the standalone mode and bind to all interface
CMD ["./echo.sh"]
